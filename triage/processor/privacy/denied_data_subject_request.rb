# frozen_string_literal: true

require_relative '../../triage/event'
require_relative '../../triage/processor'

module Triage
  class DeniedDataSubjectRequest < Processor
    react_to 'issue.open', 'issue.update'

    PRIVACY_ISSUE_ESCALATED_LABEL_NAME = 'Privacy-Issue-Escalated'
    DELETION_REQUEST_DENIED_LABEL_NAME = 'deletion request::denied'
    DATA_ACCESS_REQUEST_DENIED_LABEL_NAME = 'data-access-request::denied'
    COMMENT_BODY = <<~TEXT.chomp
      Under data privacy law, we must inform you of your right to appeal.

      Please reply within 7 calendar days if you would like to appeal our resolution of your data subject request.

      If you have any further information that would assist us in rendering a different resolution, please provide that information to dpo@gitlab.com. We will issue our determination regarding your appeal within 45 calendar days.

      If you disagree with the resolution of the appeal, you have the right to go to the Attorney General in your State.
    TEXT

    def applicable?
      event.with_project_id?(Triage::Event::DATA_SUBJECT_REQUEST_PROJECT_ID) &&
        labeled_as_denied_without_escalated? &&
        denied_entries_present_in_description?
    end

    def documentation
      'Adds label and sends message for denied data subject requests.'
    end

    def process
      add_label_and_comment
    end

    private

    def labeled_as_denied_without_escalated?
      !event.label_names.include?(PRIVACY_ISSUE_ESCALATED_LABEL_NAME) &&
        (event.label_names & [DELETION_REQUEST_DENIED_LABEL_NAME, DATA_ACCESS_REQUEST_DENIED_LABEL_NAME]).any?
    end

    def denied_entries_present_in_description?
      event.description.include?('* Country: United States') &&
        event.description.match(/\* State: (Colorado|Connecticut|Virginia|)$/).present?
    end

    def add_label_and_comment
      label_comment = %(/label ~"#{PRIVACY_ISSUE_ESCALATED_LABEL_NAME}")
      return add_comment(label_comment, append_source_link: false) unless unique_comment.no_previous_comment?

      comments = <<~MARKDOWN.chomp
        #{label_comment}
        #{COMMENT_BODY}
      MARKDOWN
      add_comment(unique_comment.wrap(comments), append_source_link: true)
    end
  end
end
